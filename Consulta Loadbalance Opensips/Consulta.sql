SELECT
    a.prefix as prefix,
    a.gwlist as gwlist,
    GROUP_CONCAT(DISTINCT r.address SEPARATOR ', ') as address
FROM opensips.dr_rules as a
INNER JOIN opensips.dr_gateways as r
    ON a.gwlist REGEXP CONCAT("[[:<:]]", r.gwid, "[[:>:]]")
    WHERE a.prefix = "083"
GROUP BY a.gwlist;