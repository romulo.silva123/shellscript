#!/bin/bash

Ftp_Address= 

Ftp_User=

Ftp_Password=

Db_User=

Db_Password=

cd /temp

apt install ftp --assume-yes;

ftp -n <<EOF
open $Ftp_Address
user $Ftp_User $Ftp_Password
get Loadbalance.txt
get Consulta.sql
EOF

IP="`ip addr show | grep global | grep -oE '((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])' | sed -n '1p'`"

Mysql_Select=$(mysql -u$Db_User -p$Db_Password < Consulta.sql)

OpensipsParan=$(grep  'modparam("tm", "fr_timer"' /etc/opensips/opensips.cfg | cut -f3,5 -d',' | sed -r 's/.$//g')


echo -e " " >> Loadbalance.txt
echo -e "IP=$IP" >> Loadbalance.txt
echo -e $Mysql_Select >> Loadbalance.txt
echo -e "Zeus Parametro=$OpensipsParan" >> Loadbalance.txt

ftp -n <<EOF
open $Ftp_Address
user $Ftp_User $Ftp_Password
put Loadbalance.txt
EOF

rm Consulta Loadbalance.txt